function radiansToDegrees(radians) {
    return radians * (180 / Math.PI);
}

function degreesToRadians(degrees) {
    return degrees / (180 / Math.PI);
}

class Point2D {
    constructor(x, y, isPolar = false) {
        this.x = x;
        this.y = y;
        this.isPolar = isPolar;
    }

    distance(p) {
        var thisC = this.toCartesian();
        var pC = p.toCartesian();
        return Math.sqrt(Math.pow(thisC.x - pC.x, 2) + Math.pow(thisC.y - pC.y, 2));
    }

    magnitude() {
        var thisC = this.toCartesian();
        return Math.sqrt(thisC.x * thisC.x + thisC.y * thisC.y);
    }

    normalize() {
        var thisC = this.toCartesian();
        var mag = thisC.magnitude();
        if (mag > 0)
            return new Point2D(thisC.x / mag, thisC.y / mag);
    }

    add(p) {
        var thisC = this.toCartesian();
        var pC = p.toCartesian();
        return new Point2D(thisC.x + pC.x, thisC.y + pC.y, false);
    }

    subtract(p) {
        var thisC = this.toCartesian();
        var pC = p.toCartesian();
        return new Point2D(thisC.x - pC.x, thisC.y - pC.y, false);
    }

    toPolar() {

        if (this.isPolar === true)
            return new Point2D(this.x, this.y, this.isPolar);

        var r = Math.sqrt(this.x * this.x + this.y * this.y);
        var fi = Math.atan2(this.y, this.x);
        var fiDegrees = radiansToDegrees(fi)
        return new Point2D(r, fiDegrees, true);
    }

    toCartesian() {
        if (this.isPolar === false)
            return new Point2D(this.x, this.y, this.isPolar);

        var fiRadians = degreesToRadians(this.y);
        var x = this.x * Math.cos(fiRadians);
        var y = this.x * Math.sin(fiRadians);
        return new Point2D(x, y, false);
    }

    toString() {
        return "(" + this.x + "," + this.y + ")";
    }
}

class Vector2D extends Point2D {

}


function toPolar(x, y) {
    var r = Math.sqrt(x * x + y * y);
    var fi = Math.atan2(y, x);
    return [r, fi];
}

function toCartesian(r, fi) {
    var x = r * Math.cos(fi);
    var y = r * Math.sin(fi);

    return [x, y];
}


function addVect(a, b) {
    return [a[0] + b[0], a[1] + b[1]];
}