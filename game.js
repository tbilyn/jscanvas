;
(function() {
    window.start = function() {
        var canvas = document.getElementById('canvas');
        window.Game = {
            canvas: canvas,
            ctx: canvas.getContext('2d'),

            Play: false,
            Position: new Point2D(100, 150),
            Speed: 0,
            CarDirection: 0,
            WheelsDirection: 0,
            MovingForward: true,
            CarRadius: 5,

            Map: {
                ObstaclesGrid: [],
                ImageFile: 'map.png',
                Image: null,
                ObstaclesGridRows: 10,
                ObstaclesGridCols: 10,
                GridBuffer: 20,
            },

            UpPressed: false,
            DownPressed: false,
            RightPressed: false,
            LeftPressed: false,

            requestFrameId: null,
            prevMilisKeyRead: 0,

            Consts: {
                MAX_SPEED: 5,
                MAX_BACK_SPEED: 2,
                MAX_TURN: 20, // angle, degrees
            }
        };

        Game.Map.Image = new Image();
        Game.Map.Image.addEventListener("load", function() {
            setup();
        }, false);
        Game.Map.Image.src = Game.Map.ImageFile;


        document.addEventListener("keydown", keyDownHandler, false);
        document.addEventListener("keyup", keyUpHandler, false);

    }

    function setup() {
        Game.ctx.drawImage(Game.Map.Image, 0, 0);
        var obstacles = GetObstacles();
        Game.Map.ObstaclesGrid = DivideByGrid(
            obstacles,
            Game.canvas.width,
            Game.canvas.height,
            Game.Map.ObstaclesGridRows,
            Game.Map.ObstaclesGridCols,
            Game.Map.GridBuffer
        );

        document.getElementById('mess').innerHTML = 'go';
        Game.Play = true;
        draw();
    }


    function draw(tFrame) {
        if (Game.Play === true)
            Game.requestFrameId = window.requestAnimationFrame(draw);

        // first check current situation

        var cellCoord = GetCell(
            Game.Position,
            Game.canvas.width,
            Game.canvas.height,
            Game.Map.ObstaclesGridRows,
            Game.Map.ObstaclesGridCols
        );
        var obstacles = Game.Map.ObstaclesGrid[cellCoord[0]][cellCoord[1]];

        var collision = FindCollision(Game.Position, Game.CarRadius, obstacles);

        // if (collision !== false) {
        //     Game.Play = false;
        //     window.cancelAnimationFrame(Game.requestFrameId);
        //     document.getElementById('mess').innerHTML = 'hit';
        // }

        if (tFrame - Game.prevMilisKeyRead > 200) {
            ReadKeyboard();
            Game.prevMilisKeyRead = tFrame;
        }

        // than calculate next situation
        if (Game.Speed != 0) {

            if (Game.MovingForward)
                Game.CarDirection += Game.WheelsDirection;
            else
                Game.CarDirection -= Game.WheelsDirection * 2;

            if (Game.CarDirection > 180)
                Game.CarDirection = -180 + Game.CarDirection - 180;
            else if (Game.CarDirection < -180)
                Game.CarDirection = 180 + Game.CarDirection + 180;

            var delta = new Point2D(Game.Speed, Game.CarDirection, true);
            var deltaCartesian = delta.toCartesian();

            Game.Position = Game.Position.add(deltaCartesian);

            // make board a tor

            if (Game.Position.x >= Game.canvas.width)
                Game.Position.x -= Game.canvas.width;
            else if (Game.Position.x < 0)
                Game.Position.x += Game.canvas.width;

            if (Game.Position.y >= Game.canvas.height)
                Game.Position.y -= Game.canvas.height;
            else if (Game.Position.y < 0)
                Game.Position.y += Game.canvas.height;
        }
        Game.ctx.drawImage(Game.Map.Image, 0, 0);
        drawCar();
    }

    function ReadKeyboard() {

        if (Game.MovingForward) {

            if (Game.UpPressed && Game.Speed < Game.Consts.MAX_SPEED)
                Game.Speed += 1;
            if (Game.UpPressed === false && Game.Speed > 0) {
                Game.Speed -= 2;
                if (Game.Speed < 0)
                    Game.Speed = 0;
            }

            if (Game.DownPressed) {
                if (Game.Speed > 0) {
                    Game.Speed -= 3;
                    if (Game.Speed < 0)
                        Game.Speed = 0;
                } else if (Game.Speed == 0) {
                    Game.MovingForward = false;
                }
            }
        } else {

            if (Game.DownPressed && Game.Speed > -Game.Consts.MAX_BACK_SPEED)
                Game.Speed -= 1;
            if (Game.DownPressed === false && Game.Speed < 0) {
                Game.Speed += 1;
                if (Game.Speed > 0)
                    Game.Speed = 0;
            }

            if (Game.UpPressed) {
                if (Game.Speed < 0) {
                    Game.Speed += 1;
                } else if (Game.Speed == 0) {
                    Game.MovingForward = true;
                }
            }
        }


        if (Game.RightPressed && Game.WheelsDirection < Game.Consts.MAX_TURN) {
            Game.WheelsDirection += 1;

        }
        if (Game.RightPressed === false && Game.WheelsDirection > 0) {
            Game.WheelsDirection -= 5;
            if (Game.WheelsDirection < 0)
                Game.WheelsDirection = 0;

        }

        if (Game.LeftPressed && Game.WheelsDirection > -Game.Consts.MAX_TURN)
            Game.WheelsDirection -= 1;
        if (Game.LeftPressed === false && Game.WheelsDirection < 0) {
            Game.WheelsDirection += 5;
            if (Game.WheelsDirection > 0)
                Game.WheelsDirection = 0;
        }
    }

    function drawCar() {
        Game.ctx.beginPath();
        Game.ctx.arc(Game.Position.x, Game.Position.y, Game.CarRadius, 0, Math.PI * 2);
        Game.ctx.fillStyle = "#0000ff";
        Game.ctx.fill();
        Game.ctx.closePath();

        var delta = new Point2D(Game.CarRadius * 2, Game.CarDirection, true);
        var dc = delta.toCartesian();
        var t = Game.Position.add(dc);
        Game.ctx.beginPath();
        Game.ctx.moveTo(Game.Position.x, Game.Position.y);
        Game.ctx.lineTo(t.x, t.y);
        Game.ctx.fillStyle = "#0000ff";
        Game.ctx.stroke();
    }

    function FindCollision(point, radius, obstacles) {
        for (var i = 0; i < obstacles.length; i++)
            if (point.distance(obstacles[i]) < radius)
                return obstacles[i];
        return false;
    }

    function keyDownHandler(e) {
        if (e.keyCode == 39) {
            Game.RightPressed = true;
        } else if (e.keyCode == 37) {
            Game.LeftPressed = true;
        } else if (e.keyCode == 38) {
            Game.UpPressed = true;
        } else if (e.keyCode == 40) {
            Game.DownPressed = true;
        }
    }

    function keyUpHandler(e) {
        if (e.keyCode == 39) {
            Game.RightPressed = false;
        } else if (e.keyCode == 37) {
            Game.LeftPressed = false;
        } else if (e.keyCode == 38) {
            Game.UpPressed = false;
        } else if (e.keyCode == 40) {
            Game.DownPressed = false;
        }
    }

    function GetObstacles() {
        var width = Game.canvas.width;
        var height = Game.canvas.height;
        var obstacles = [];
        // @todo: do not forget to add canvas borders as obstacles
        for (var i = 0; i < width; i++)
            for (var j = 0; j < height; j++) {
                var rgb = Game.ctx.getImageData(i, j, 1, 1).data;
                if (rgb[0] == 0 && rgb[1] == 0 && rgb[2] == 0) {
                    var p = new Point2D(i, j);
                    obstacles.push(p);
                }
            }
        return obstacles;
    }
})();