// require mathlib.js

function DivideByGrid(points, width, height, rows, cols, borderBuffer) {

    var result = [];
    for (var i = 0; i < rows; i++) {
        result.push([]);
        for (var j = 0; j < cols; j++)
            result[i].push([]);
    }
    var colWidth = width / cols;
    var rowHeight = height / rows;

    for (var i = 0; i < points.length; i++) {
        var p = points[i];
        var cells = getObstaclesCells(p, colWidth, rowHeight, rows, cols, borderBuffer);
        for (var j = 0; j < cells.length; j++) {
            result[cells[j][0]][cells[j][1]].push(p);
        }
    }

    return result;
}

function GetCell(point, width, height, rows, cols) {
    var colWidth = width / cols;
    var rowHeight = height / rows;

    var col = Math.floor(point.x / colWidth);
    var r = point.x % colWidth;

    var row = Math.floor(point.y / rowHeight);
    r = point.y % rowHeight;

    return [row, col];
}

function getObstaclesCells(point, colWidth, rowHeight, rows, cols, borderBuffer) {

    var col1 = Math.floor(point.x / colWidth);
    var r = point.x % colWidth;

    var col2 = null;
    if (r < borderBuffer && col1 !== 0)
        col2 = col1 - 1;
    if (r > colWidth - borderBuffer && col1 !== cols - 1)
        col2 = col1 + 1;

    var row1 = Math.floor(point.y / rowHeight);
    r = point.y % rowHeight;

    var row2 = null;
    if (r < borderBuffer && row1 !== 0)
        row2 = row1 - 1;
    if (r > rowHeight - borderBuffer && row1 !== rows - 1)
        row2 = row1 + 1;

    var res = [];
    var rowsAr = [row1, row2];
    var colsAr = [col1, col2];
    for (var i = 0; i < 2; i++)
        for (var j = 0; j < 2; j++) {
            if (rowsAr[i] !== null && colsAr[j] !== null)
                res.push([rowsAr[i], colsAr[j]]);
        }
    return res;
}